rwc [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/rwc?status.svg)](http://godoc.org/gitlab.com/opennota/rwc) [![Pipeline status](https://gitlab.com/opennota/rwc/badges/master/pipeline.svg)](https://gitlab.com/opennota/rwc/commits/master)
===

Package rwc provides a pseudo-Russian word constructor.

## Install

    go get -u gitlab.com/opennota/rwc
    go install gitlab.com/opennota/rwc/cmd/nonstop@latest

## Use

``` Go
package main
import (
	"fmt"
	"gitlab.com/opennota/rwc"
)
func main() {
	fmt.Println(rwc.Word(7))
	fmt.Println(rwc.WordMask("зVC...я"))
}
```

## See also

http://kirsanov.com/rwc/ ([Eng](http://www.kirsanov.com/rwc/english.html))

